# app

## Setup

Download [NodeJS](https://nodejs.org/en/) version 12+ and [Android Studio](https://developer.android.com/studio)

Install yarn globally

    npm install --global yarn

Finally, run

    yarn

Run the linter

    yarn lint

Fix problems found by the linter

    yarn lint:fix

> With VS Code -> <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>P</kbd> -> "Debug: Toggle Auto Attach" set to "Only with Flag"

Make sure to install these VS Code Extensions (restart VSC after installation):

- ESLint `dbaeumer.vscode-eslint`
- Prettier - Code formatter `esbenp.prettier-vscode`

For prettier you can activate auto formatting by going to File -> Preferences -> Settings, then search for `Editor: Format On Save` and activate it.

Also set VS Codes indentation to use two spaces (press <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>P</kbd> and type `Indent Using Spaces`) otherwise Prettier and ESLint will bitch about it.

If you have both the backend and shopping-app fodlers open in vscode create a file in .vscode/seettings.json (next to the project folders) with the following content:

```json
{
  "eslint.workingDirectories": ["shopping-app", "backend"]
}
```

## Run

Start a VD through Android Studio -> Configure -> AVD Manager and run

    yarn android

or

    yarn ios
