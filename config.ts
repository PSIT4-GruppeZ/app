const isDev = process.env.NODE_ENV === 'development';

// eslint-disable-next-line import/prefer-default-export
export const API_URL = isDev ? 'http://10.0.2.2:3001' : 'https://main.psit4.pve01.northcode.ch';
