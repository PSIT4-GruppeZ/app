import { StackNavigationProp } from '@react-navigation/stack';
import React from 'react';
import CUDList from '../../components/CUDList/SimpleCUDList';
import IBaseListItem from '../../interfaces/IBaseListItem';
import { ShopsParamList } from '../../types';

type ShopsCategoryOrderScreenNavigationProp = StackNavigationProp<ShopsParamList, 'ShopsCategoryOrderScreen'>;

type Props = {
  navigation: ShopsCategoryOrderScreenNavigationProp;
};

const ShopsScreen: React.FC<Props> = ({ navigation }) => {
  const onShopClick = async (item: IBaseListItem) => {
    navigation.navigate('ShopsCategoryOrderScreen', item);
  };

  return <CUDList apiPath="/api/store" itemType="shop" onItemClick={onShopClick} />;
};

export default ShopsScreen;
