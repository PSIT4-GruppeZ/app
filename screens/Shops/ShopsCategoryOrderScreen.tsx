/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-non-null-assertion */
import React, { useCallback, useState } from 'react';
import { Col, Container, Content, Grid, ListItem, Text } from 'native-base';
import { StackNavigationProp } from '@react-navigation/stack';
import DraggableFlatList, { RenderItemParams } from 'react-native-draggable-flatlist';
import { ShopsParamList } from '../../types';
import IBaseListItem from '../../interfaces/IBaseListItem';
import { useGet, useMutation } from '../../hooks/useApi';
import style from '../../components/UDListItem/UDListItem.style';
import IShop from '../../interfaces/IShop';
import IShoppingList from '../../interfaces/IShoppingList';

type ShopsCategoryOrderScreenNavigationProp = StackNavigationProp<ShopsParamList, 'ShopsCategoryOrderScreen'>;

type Props = {
  navigation: ShopsCategoryOrderScreenNavigationProp;
  route: Record<string, IBaseListItem>;
};

interface Category {
  id: string;
  name: string;
}

interface CategoryOrderUpdate {
  id: string;
  categoryOrder: string[];
}

const ShopsCategoryOrderScreen: React.FC<Props> = ({ navigation, route }) => {
  const { id, name } = route.params;

  const [categories, getCategories, loadingCategories] = useGet<Category[]>('/api/category');
  const [shop, getShop, loadingShop] = useGet<IShop>(`/api/store/${id}`);
  const [mutate] = useMutation<CategoryOrderUpdate>();

  const [orderedCategories, setOrder] = useState<Category[]>([]);

  const loading = loadingCategories || loadingShop;

  if (orderedCategories.length === 0 && !loading && shop && categories) {
    const order: Category[] = [];
    shop.categoryOrder.forEach((categoryId) => {
      const cat = categories.find((category) => category.id === categoryId);
      if (cat) order.push(cat);
    });
    setOrder(order);
  }

  const renderItem = useCallback(({ item, index, drag, isActive }: RenderItemParams<Category>) => {
    return (
      <ListItem onPressIn={drag}>
        <Grid>
          <Col style={style.container}>
            <Text style={style.leftAlign}>{item.name}</Text>
          </Col>
        </Grid>
      </ListItem>
    );
  }, []);

  const updateOrder = ({ data }: { data: Category[] }) => {
    setOrder(data);
    const x = data.map((entry) => entry.id);
    if (shop) shop.categoryOrder = x;
    mutate('PUT', `/api/store/${id}/order`, {
      id,
      categoryOrder: x,
    });
  };

  return (
    <>
      <Container>
        <Content>
          {!loading ? (
            <DraggableFlatList
              data={orderedCategories}
              renderItem={renderItem}
              keyExtractor={(item, index) => `draggable-item-${item.id}`}
              onDragEnd={updateOrder}
            />
          ) : (
            <Text>Loading...</Text>
          )}
        </Content>
      </Container>
    </>
  );
};

export default ShopsCategoryOrderScreen;
