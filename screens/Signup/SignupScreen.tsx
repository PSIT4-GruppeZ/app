import React, { useCallback, useState } from 'react';

import { Container, Text, Content, Form, Item, Input, Button, Toast, Header, Body, Title } from 'native-base';
import { StackNavigationProp } from '@react-navigation/stack';
import { API_URL } from '../../config';
import { RootStackParamList } from '../../types';

import { ErrorResponse } from '../Login/LoginScreen';

type SignupScreenNavigationProp = StackNavigationProp<RootStackParamList, 'Signup'>;

type Props = {
  navigation: SignupScreenNavigationProp;
};

type ValidationError = {
  param: string;
  msg: string;
  nestedErrors: ValidationError[];
  location?: undefined;
  value?: undefined;
};

interface ValidationResponse {
  errors: ValidationError[];
}

const isValidationResponse = (error: ValidationResponse | ErrorResponse): error is ValidationResponse => {
  return (error as ValidationResponse).errors !== undefined;
};

const SignupScreen: React.FC<Props> = ({ navigation }) => {
  const [username, setUsername] = useState({ value: '', error: false });
  const [email, setEmail] = useState({ value: '', error: false });
  const [pw, setPw] = useState({ value: '', error: false });
  const [pwConfirm, setPwConfirm] = useState({ value: '', error: false });

  const signup = useCallback(async () => {
    if (!username.value) {
      setUsername((u) => {
        return { ...u, error: true };
      });
    }
    if (!email.value) {
      setEmail((e) => {
        return { ...e, error: true };
      });
    }
    if (!pw.value) {
      setPw((p) => {
        return { ...p, error: true };
      });
    }
    if (!pwConfirm.value) {
      setPwConfirm((p) => {
        return { ...p, error: true };
      });
    }
    if (pwConfirm.value === pw.value) {
      try {
        const response = await fetch(`${API_URL}/api/auth/signup`, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            username: username.value,
            email: email.value,
            password: pw.value,
          }),
        });
        if (response.ok) {
          // const user: UserResponse = await response.json();
          navigation.navigate('Login');
        } else {
          const error: ErrorResponse | ValidationResponse = await response.json();
          if (isValidationResponse(error)) {
            error.errors.forEach((e) => {
              Toast.show({
                text: `${e.param}: ${e.msg} - "${e.value}"`,
                buttonText: 'Okay',
                duration: 3000,
              });
            });
          } else {
            Toast.show({
              text: error.message,
              buttonText: 'Okay',
              duration: 3000,
            });
          }
        }
      } catch (e) {
        Toast.show({
          text: 'Unexpected error, contact sys admin pls, @morfoli',
          buttonText: 'Ok, ew',
          duration: 3000,
        });
      }
    } else {
      Toast.show({
        text: 'Passwords must match',
        buttonText: 'Okay',
        duration: 3000,
      });
      setPw((p) => {
        return { ...p, error: true };
      });
      setPwConfirm({ value: '', error: true });
    }
  }, [email.value, navigation, pw.value, pwConfirm.value, username.value]);

  return (
    <Container>
      <Header>
        <Body>
          <Title>Signup</Title>
        </Body>
      </Header>
      <Content>
        <Form>
          <Item error={username.error}>
            <Input
              placeholder="Username"
              value={username.value}
              textContentType="username"
              autoCapitalize="none"
              autoCorrect={false}
              autoFocus
              onChangeText={(t) =>
                setUsername((u) => {
                  return { ...u, value: t };
                })
              }
            />
          </Item>
          <Item error={email.error}>
            <Input
              placeholder="Email"
              value={email.value}
              autoCorrect={false}
              textContentType="emailAddress"
              autoCapitalize="none"
              onChangeText={(t) =>
                setEmail((e) => {
                  return { ...e, value: t };
                })
              }
            />
          </Item>
          <Item error={pw.error}>
            <Input
              placeholder="Password"
              textContentType="newPassword"
              secureTextEntry
              autoCorrect={false}
              autoCapitalize="none"
              value={pw.value}
              onChangeText={(t) =>
                setPw((p) => {
                  return { ...p, value: t };
                })
              }
            />
          </Item>
          <Item error={pwConfirm.error} last>
            <Input
              placeholder="Confirm password"
              secureTextEntry
              autoCorrect={false}
              autoCapitalize="none"
              value={pwConfirm.value}
              onChangeText={(t) =>
                setPwConfirm((p) => {
                  return { ...p, value: t };
                })
              }
            />
          </Item>
        </Form>
        <Button full danger onPress={signup}>
          <Text>Signup</Text>
        </Button>
      </Content>
    </Container>
  );
};

export default SignupScreen;
