import { StackNavigationProp } from '@react-navigation/stack';
import React from 'react';
import LoyalityCardCUDList from '../../components/CUDList/LoyalityCardCUDList';
import IBaseListItem from '../../interfaces/IBaseListItem';
import { LoyalityCardParamList } from '../../types';

type LoginScreenNavigationProp = StackNavigationProp<LoyalityCardParamList, 'LoyalityCardScreen'>;

type Props = {
  navigation: LoginScreenNavigationProp;
};

const LoyalityCardScreen: React.FC<Props> = ({ navigation }) => {
  const onItemClick = (item: IBaseListItem): void => {
    navigation.navigate('LoyalityCardDetailScreen', item);
  };

  return <LoyalityCardCUDList apiPath="/api/loyalitycard" itemType="loyality card" onItemClick={onItemClick} />;
};

export default LoyalityCardScreen;
