import React, { useEffect } from 'react';
import { Container, Text } from 'native-base';
import Barcode from 'react-native-barcode-builder';
import { StackNavigationProp } from '@react-navigation/stack';
import { LoyalityCardParamList } from '../../types';
import ILoyalityCardListItem from '../../interfaces/ILoyalityCardListItem';

type LoginScreenNavigationProp = StackNavigationProp<LoyalityCardParamList, 'LoyalityCardDetailScreen'>;

type Props = {
  navigation: LoginScreenNavigationProp;
  route: Record<string, ILoyalityCardListItem>;
};

const LoyalityCardDetailScreen: React.FC<Props> = ({ navigation, route }) => {
  const { name, barcode } = route.params;

  useEffect(() => {
    navigation.setOptions({ headerTitle: name });
  });

  const validateBarcode = (checkBarcode: string | string[]): boolean => {
    let even = 0;
    for (let i = 1; i < 12; i += 2) {
      even += parseInt(checkBarcode[i], 10);
    }
    even *= 3;

    let odd = 0;
    for (let i = 0; i < 12; i += 2) {
      odd += parseInt(checkBarcode[i], 10);
    }
    const checksum = 10 - ((odd + even) % 10);

    if (checksum !== parseInt(checkBarcode[12], 10)) {
      return false;
    }
    return true;
  };

  return (
    <Container>
      {validateBarcode(barcode) ? (
        <Barcode value={barcode} format="EAN13" flat />
      ) : (
        <Text>EAN-13 barcode wrong format</Text>
      )}
      <Text>Customer id: {barcode}</Text>
    </Container>
  );
};

export default LoyalityCardDetailScreen;
