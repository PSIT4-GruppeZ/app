import React from 'react';
import ItemsCUDList from '../../components/CUDList/ItemsCUDList';

const ItemsScreen: React.FC = () => {
  return <ItemsCUDList apiPath="/api/item" itemType="item" />;
};

export default ItemsScreen;
