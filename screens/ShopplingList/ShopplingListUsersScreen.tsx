import { StackNavigationProp } from '@react-navigation/stack';
import React, { useEffect } from 'react';
import ShoppingListUsersCUDList from '../../components/CUDList/ShoppingListUsersCUDList';
import IBaseListItem from '../../interfaces/IBaseListItem';
import { ShoppingListParamList } from '../../types';

type LoginScreenNavigationProp = StackNavigationProp<ShoppingListParamList, 'ShoppingListUsersScreen'>;

type Props = {
  navigation: LoginScreenNavigationProp;
  route: Record<string, IBaseListItem>;
};

const ShoppingListUsersScreen: React.FC<Props> = ({ navigation, route }) => {
  const { id, name } = route.params;

  useEffect(() => {
    navigation.setOptions({ headerTitle: `${name}: Users` });
  });

  return <ShoppingListUsersCUDList apiPath={`/api/cart/${id}`} itemType="cartUser" />;
};

export default ShoppingListUsersScreen;
