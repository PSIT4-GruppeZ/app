import { StackNavigationProp } from '@react-navigation/stack';
import React from 'react';
import ShoppingListCUDList from '../../components/CUDList/ShoppingListCUDList';
import IBaseListItem from '../../interfaces/IBaseListItem';
import { ShoppingListParamList } from '../../types';

type LoginScreenNavigationProp = StackNavigationProp<ShoppingListParamList, 'ShoppingListScreen'>;

type Props = {
  navigation: LoginScreenNavigationProp;
};

const ShoppingListScreen: React.FC<Props> = ({ navigation }) => {
  const onItemClick = (item: IBaseListItem): void => {
    navigation.navigate('ShoppingListDetailScreen', item);
  };

  return <ShoppingListCUDList apiPath="/api/cart" itemType="cart" onItemClick={onItemClick} />;
};

export default ShoppingListScreen;
