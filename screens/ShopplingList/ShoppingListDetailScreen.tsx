import { StackNavigationProp } from '@react-navigation/stack';
import React, { useEffect } from 'react';
import ShoppingListDetailCUDList from '../../components/CUDList/ShoppingListDetailCUDList';
import IBaseListItem from '../../interfaces/IBaseListItem';
import { ShoppingListParamList } from '../../types';

type LoginScreenNavigationProp = StackNavigationProp<ShoppingListParamList, 'ShoppingListDetailScreen'>;

type Props = {
  navigation: LoginScreenNavigationProp;
  route: Record<string, IBaseListItem>;
};

const ShoppingListDetailScreen: React.FC<Props> = ({ navigation, route }) => {
  const { id, name } = route.params;

  useEffect(() => {
    navigation.setOptions({ headerTitle: name });
  });

  const shareNavigate = (): void => {
    navigation.navigate('ShoppingListUsersScreen', route.params);
  };

  return <ShoppingListDetailCUDList apiPath={`/api/cart/${id}`} itemType="cartItem" shareNavigate={shareNavigate} />;
};

export default ShoppingListDetailScreen;
