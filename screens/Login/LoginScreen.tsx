import React, { useCallback, useEffect, useState } from 'react';
import { Container, Text, Content, Form, Item, Input, Button, Toast, Header, Body, Title } from 'native-base';
import { StackNavigationProp } from '@react-navigation/stack';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { RootStackParamList } from '../../types';
import { API_URL } from '../../config';

type LoginScreenNavigationProp = StackNavigationProp<RootStackParamList, 'Login'>;

type Props = {
  navigation: LoginScreenNavigationProp;
};

export interface ErrorResponse {
  message: string;
}

const LoginScreen: React.FC<Props> = ({ navigation }) => {
  // const { loading, error, data = [] } = useFetch(`${config.backend}/api/cart`);

  const [username, setUsername] = useState({ value: '', error: false });
  const [pw, setPw] = useState({ value: '', error: false });

  useEffect(() => {
    AsyncStorage.getItem('token').then((token) => {
      if (token) {
        navigation.replace('Root');
      }
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const login = useCallback(async () => {
    if (!username.value) {
      setUsername((u) => {
        return { ...u, error: true };
      });
    }
    if (!pw.value) {
      setPw((p) => {
        return { ...p, error: true };
      });
    }
    try {
      const response = await fetch(`${API_URL}/api/auth/login`, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          usernameOrEmail: username.value,
          password: pw.value,
        }),
      });
      if (response.ok) {
        const json = await response.json();
        await AsyncStorage.setItem('token', json.token);
        navigation.replace('Root');
      } else {
        const error: ErrorResponse = await response.json();
        Toast.show({
          text: error.message,
          buttonText: 'Okay',
          duration: 3000,
        });
      }
    } catch (e) {
      Toast.show({
        text: 'Login failed',
        buttonText: 'Okay',
        duration: 3000,
      });
    }
  }, [navigation, pw.value, username.value]);

  return (
    <Container>
      <Header>
        <Body>
          <Title>Login</Title>
        </Body>
      </Header>
      <Content>
        <Form>
          <Item error={username.error}>
            <Input
              placeholder="Username/Email"
              value={username.value}
              textContentType="emailAddress"
              autoCapitalize="none"
              autoCorrect={false}
              autoFocus
              onChangeText={(t) =>
                setUsername((u) => {
                  return { ...u, value: t };
                })
              }
            />
          </Item>
          <Item error={pw.error}>
            <Input
              placeholder="Password"
              textContentType="password"
              secureTextEntry
              autoCorrect={false}
              autoCapitalize="none"
              value={pw.value}
              onChangeText={(t) =>
                setPw((p) => {
                  return { ...p, value: t };
                })
              }
            />
          </Item>
        </Form>
        <Button full dark onPress={login}>
          <Text>Login</Text>
        </Button>
        <Button onPress={() => navigation.navigate('Signup')} full danger>
          <Text>Signup</Text>
        </Button>
        <Text>Connecting to {API_URL}</Text>
      </Content>
    </Container>
  );
};

export default LoginScreen;
