import React from 'react';
import CUDList from '../../components/CUDList/SimpleCUDList';

const CategoryScreen: React.FC = () => {
  return <CUDList apiPath="/api/category" itemType="category" />;
};

export default CategoryScreen;
