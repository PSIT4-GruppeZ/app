/* eslint-disable react/style-prop-object */
import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { Root } from 'native-base';

import useCachedResources from './hooks/useCachedResources';
import Navigation from './navigation';

const App: React.FC = () => {
  const isLoadingComplete = useCachedResources();

  if (!isLoadingComplete) {
    return null;
  }
  return (
    <SafeAreaProvider>
      <Root>
        <Navigation />
      </Root>
      <StatusBar style="light" hidden />
    </SafeAreaProvider>
  );
};

export default App;
