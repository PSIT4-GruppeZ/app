import IBaseListItem from './interfaces/IBaseListItem';

export type RootStackParamList = {
  Root: undefined;
  Signup: undefined;
  Login: undefined;
  NotFound: undefined;
};

export type DrawerParamList = {
  Shoppinglists: undefined;
  Categories: undefined;
  LoyalityCard: undefined;
  Shops: undefined;
  Items: undefined;
};

export type CategoryParamList = {
  CategoryScreen: undefined;
};

export type ShoppingListParamList = {
  ShoppingListScreen: undefined;
  ShoppingListDetailScreen: IBaseListItem;
  ShoppingListUsersScreen: IBaseListItem;
};

export type ShopsParamList = {
  ShopsScreen: undefined;
  ShopsCategoryOrderScreen: IBaseListItem;
};

export type ItemsParamList = {
  ItemsScreen: undefined;
};

export type LoyalityCardParamList = {
  LoyalityCardScreen: undefined;
  LoyalityCardDetailScreen: IBaseListItem;
};
