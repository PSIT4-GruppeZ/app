import * as Linking from 'expo-linking';

export default {
  prefixes: [Linking.makeUrl('/')],
  config: {
    screens: {
      Login: {
        screens: {
          LoginScreen: 'login',
        },
      },
      Root: {
        screens: {
          Categories: {
            screens: {
              CategoryScreen: 'Categories',
            },
          },
          ShoppingList: {
            screens: {
              ShoppingListScreen: 'ShoppingList',
            },
          },
          Shops: {
            screens: {
              ShopsScreen: 'Shops',
            },
          },
          Items: {
            screens: {
              ItemsScreen: 'Items',
            },
          },
          LoyalityCard: {
            screens: {
              LoyalityCardScreen: 'LoyalityCard',
            },
          },
        },
      },
      NotFound: '*',
    },
  },
};
