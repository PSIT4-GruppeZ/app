import { createStackNavigator, StackNavigationProp } from '@react-navigation/stack';
import * as React from 'react';
import {
  createDrawerNavigator,
  DrawerContentComponentProps,
  DrawerContentOptions,
  DrawerContentScrollView,
  DrawerItem,
  DrawerItemList,
} from '@react-navigation/drawer';
import AsyncStorage from '@react-native-async-storage/async-storage';
import CategoryScreen from '../screens/Category/CategoryScreen';
import {
  DrawerParamList,
  CategoryParamList,
  ShoppingListParamList,
  ShopsParamList,
  RootStackParamList,
  LoyalityCardParamList,
  ItemsParamList,
} from '../types';
import ShoppingListScreen from '../screens/ShopplingList/ShopplingListScreen';
import ShopsScreen from '../screens/Shops/ShopsScreen';
import ItemsScreen from '../screens/Items/Items';
import ShoppingListDetailScreen from '../screens/ShopplingList/ShoppingListDetailScreen';
import LoyalityCardScreen from '../screens/LocalityCard/LoyalityCardScreen';
import ShoppingListUsersScreen from '../screens/ShopplingList/ShopplingListUsersScreen';
import LoyalityCardDetailScreen from '../screens/LocalityCard/LoyalityCardDetailScreen';
import ShopsCategoryOrderScreen from '../screens/Shops/ShopsCategoryOrderScreen';

const Drawer = createDrawerNavigator<DrawerParamList>();
const ShoppingListStack = createStackNavigator<ShoppingListParamList>();
const CategoryStack = createStackNavigator<CategoryParamList>();
const LoyalityCardStack = createStackNavigator<LoyalityCardParamList>();
const ShopsStack = createStackNavigator<ShopsParamList>();
const ItemsStack = createStackNavigator<ItemsParamList>();

type CustomDrawerProps = {
  stackNavigation: DrawerNavigationProp;
  drawerProps: DrawerContentComponentProps<DrawerContentOptions>;
};

type DrawerNavigationProp = StackNavigationProp<RootStackParamList, 'Root'>;

type DrawerNavigatorProps = {
  navigation: DrawerNavigationProp;
};

const CategoryNavigator: React.FC = () => {
  return (
    <CategoryStack.Navigator>
      <CategoryStack.Screen name="CategoryScreen" component={CategoryScreen} options={{ headerTitle: 'Categories' }} />
    </CategoryStack.Navigator>
  );
};

const ShoppingListNavigator: React.FC = () => {
  return (
    <ShoppingListStack.Navigator>
      <ShoppingListStack.Screen
        name="ShoppingListScreen"
        component={ShoppingListScreen}
        options={{ headerTitle: 'Shoppinglist' }}
      />
      <ShoppingListStack.Screen
        name="ShoppingListDetailScreen"
        component={ShoppingListDetailScreen}
        options={{ headerTitle: 'Shoppinglistdetail' }}
      />
      <ShoppingListStack.Screen
        name="ShoppingListUsersScreen"
        component={ShoppingListUsersScreen}
        options={{ headerTitle: 'Shoppinglistusers' }}
      />
    </ShoppingListStack.Navigator>
  );
};

const ShopsNavigator: React.FC = () => {
  return (
    <ShopsStack.Navigator>
      <ShopsStack.Screen name="ShopsScreen" component={ShopsScreen} options={{ headerTitle: 'Shops' }} />
      <ShopsStack.Screen
        name="ShopsCategoryOrderScreen"
        component={ShopsCategoryOrderScreen}
        options={{ headerTitle: 'Reorder categories' }}
      />
    </ShopsStack.Navigator>
  );
};

const ItemsNavigator: React.FC = () => {
  return (
    <ShopsStack.Navigator>
      <ItemsStack.Screen name="ItemsScreen" component={ItemsScreen} options={{ headerTitle: 'Items' }} />
    </ShopsStack.Navigator>
  );
};

const LoyalityCardNavigator: React.FC = () => {
  return (
    <LoyalityCardStack.Navigator>
      <LoyalityCardStack.Screen
        name="LoyalityCardScreen"
        component={LoyalityCardScreen}
        options={{ headerTitle: 'Loyality Card' }}
      />
      <LoyalityCardStack.Screen
        name="LoyalityCardDetailScreen"
        component={LoyalityCardDetailScreen}
        options={{ headerTitle: 'Loyaliy Card Detail' }}
      />
    </LoyalityCardStack.Navigator>
  );
};

const CustomDrawerContent: React.FC<CustomDrawerProps> = (props) => {
  const { stackNavigation, drawerProps } = props;
  return (
    <DrawerContentScrollView {...props}>
      <DrawerItemList {...drawerProps} />
      <DrawerItem
        label="Logout"
        onPress={() => {
          AsyncStorage.removeItem('token');
          stackNavigation.replace('Login');
        }}
      />
    </DrawerContentScrollView>
  );
};

const DrawerNavigator: React.FC<DrawerNavigatorProps> = (props) => {
  const { navigation } = props;
  return (
    <Drawer.Navigator
      initialRouteName="Shoppinglists"
      drawerContent={(drawerProps) => <CustomDrawerContent drawerProps={drawerProps} stackNavigation={navigation} />}
    >
      <Drawer.Screen name="Shoppinglists" component={ShoppingListNavigator} />
      <Drawer.Screen name="Categories" component={CategoryNavigator} />
      <Drawer.Screen name="LoyalityCard" component={LoyalityCardNavigator} />
      <Drawer.Screen name="Shops" component={ShopsNavigator} />
      <Drawer.Screen name="Items" component={ItemsNavigator} />
    </Drawer.Navigator>
  );
};

export default DrawerNavigator;
