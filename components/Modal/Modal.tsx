import { View, Text, Button } from 'native-base';
import React from 'react';
import { Modal } from 'react-native';
import ModalStyle from './Modal.style';

interface ICustomModal {
  modalVisible: boolean;
  modalTitle: string;
  children?: JSX.Element;
  handleClose: () => void;
  handleSave: () => void;
}

const CustomModal: React.FC<ICustomModal> = (props) => {
  const { modalVisible, modalTitle, children, handleClose, handleSave } = props;

  return (
    <Modal transparent animationType="fade" visible={modalVisible} presentationStyle="overFullScreen">
      <View style={ModalStyle.centeredView}>
        <View style={ModalStyle.modalView}>
          <Text>{modalTitle}</Text>
          {children}
          <Button onPress={handleClose} full light style={ModalStyle.marginTop}>
            <Text>Cancel</Text>
          </Button>
          <Button onPress={handleSave} full>
            <Text>Save</Text>
          </Button>
        </View>
      </View>
    </Modal>
  );
};

export default CustomModal;
