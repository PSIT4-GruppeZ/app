import React, { useState } from 'react';
import { Col, Text, Grid, Icon, Button } from 'native-base';
import { TextInput } from 'react-native';
// import ICategory from '../../interfaces/ICategory';
import style from './CategoryItem.style';
import CustomModal from '../Modal/Modal';

interface ICategory {
  title: string;
}

interface CategoryItemProps {
  item: ICategory;
  deleteCategory: (category: ICategory) => void;
  updateCategory: (category: ICategory) => void;
}

const CategoryItem: React.FC<CategoryItemProps> = (props) => {
  const { item, deleteCategory, updateCategory } = props;
  const [updateModalVisible, toggleUpdateModal] = useState(false);
  const [updateTitle, setUpdateTitle] = useState('');

  const openUpdateModal = (category: ICategory): void => {
    setUpdateTitle(category.title);
    toggleUpdateModal(true);
  };

  const closeUpdateModal = (): void => {
    toggleUpdateModal(false);
  };

  const update = async (): Promise<void> => {
    const updateItem = { ...item, title: updateTitle };
    await updateCategory(updateItem);
    closeUpdateModal();
  };

  return (
    <>
      <Grid>
        <Col style={style.container}>
          <Text style={style.leftAlign}>{item.title}</Text>
        </Col>
        <Col style={style.iconCol}>
          <Icon name="pencil" onPress={() => openUpdateModal(item)} />
        </Col>
        <Col style={style.iconCol}>
          <Icon name="trash" onPress={() => deleteCategory(item)} />
        </Col>
      </Grid>
      <CustomModal modalVisible={updateModalVisible} modalTitle="Update Category">
        <>
          <TextInput
            onChangeText={setUpdateTitle}
            value={updateTitle}
            style={style.modalInput}
            placeholder={`Rename ${item.title}`}
            autoFocus
          />
          <Button onPress={closeUpdateModal} full light style={style.marginTop}>
            <Text>Cancel</Text>
          </Button>
          <Button onPress={update} full>
            <Text>Save</Text>
          </Button>
        </>
      </CustomModal>
    </>
  );
};

export default CategoryItem;
