import React, { useState } from 'react';
import { Button, Text, Container, Content, Footer, Icon, List, Toast, Picker } from 'native-base';
import { TextInput } from 'react-native';
import CustomModal from '../Modal/Modal';
import style from './CUDList.style';
import IBaseListItem from '../../interfaces/IBaseListItem';
import { useGet, useMutation } from '../../hooks/useApi';
import ItemsUDListItem from '../UDListItem/ItemsUDListItem';

interface CUDListProps {
  apiPath: string;
  itemType: string;
}

export interface Item {
  id: string;
  name: string;
  category: {
    id: string;
    name: string;
  };
}

const ItemsCUDList: React.FC<CUDListProps> = ({ apiPath, itemType }) => {
  const [addModalVisible, toggleAddModal] = useState(false);
  const [addName, setAddName] = useState('');
  const [category, setCategory] = useState<IBaseListItem>();

  const [items, getItems, loading] = useGet<Item[]>(apiPath);
  const [categories] = useGet<IBaseListItem[]>('/api/category');
  const [mutate] = useMutation<IBaseListItem>();

  const closeAddModal = (): void => {
    setAddName('');
    toggleAddModal(false);
  };

  const handleSave = async () => {
    try {
      await mutate('POST', apiPath, { name: addName, categoryId: category?.id ?? '' });
    } catch (e) {
      Toast.show({ text: `Unable to add a new ${itemType}`, duration: 3000 });
    } finally {
      setAddName('');
      toggleAddModal(false);
      getItems();
    }
  };

  const handleUpdate = async (item: IBaseListItem, newCategory?: IBaseListItem) => {
    try {
      await mutate('PUT', `${apiPath}/${item.id}`, { name: item.name, categoryId: newCategory?.id ?? '' });
    } catch (e) {
      Toast.show({ text: `Unable to update ${itemType}`, duration: 3000 });
    } finally {
      setAddName('');
      toggleAddModal(false);
      getItems();
    }
  };

  const handleDelete = async (item: IBaseListItem) => {
    try {
      await mutate('DELETE', `${apiPath}/${item.id}`);
    } catch (e) {
      Toast.show({ text: `Unable to delete ${itemType}`, duration: 3000 });
    } finally {
      setAddName('');
      toggleAddModal(false);
      getItems();
    }
  };

  return (
    <>
      <Container>
        <Content>
          <List>
            {items &&
              categories &&
              items.map((cat) => (
                <ItemsUDListItem
                  listItem={cat}
                  categories={categories}
                  deleteItem={handleDelete}
                  updateItem={handleUpdate}
                  modalTitle={`Update ${itemType}`}
                  key={cat.id}
                />
              ))}
            {loading && <Text>Loading...</Text>}
          </List>
        </Content>
        <Footer>
          <Button large primary onPress={() => toggleAddModal(true)}>
            <Icon name="add" />
          </Button>
        </Footer>
      </Container>
      <CustomModal
        modalVisible={addModalVisible}
        modalTitle={`Add ${itemType}`}
        handleClose={closeAddModal}
        handleSave={handleSave}
      >
        <>
          <TextInput
            onChangeText={setAddName}
            value={addName}
            style={style.modalInput}
            placeholder={`New ${itemType} name`}
            autoFocus
          />
          {categories && (
            <Picker
              selectedValue={category}
              mode="dropdown"
              placeholder="Select category"
              placeholderStyle={{ color: '#bfc6ea' }}
              placeholderIconColor="#007aff"
              onValueChange={(item) => setCategory(item)}
              style={{ marginTop: 20, width: 200, height: 50 }}
            >
              {categories.map((item) => (
                <Picker.Item label={item.name} value={item} key={item.id} />
              ))}
            </Picker>
          )}
        </>
      </CustomModal>
    </>
  );
};

export default ItemsCUDList;
