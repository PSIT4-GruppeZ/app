/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useState } from 'react';
import { Button, Text, Container, Content, Footer, Icon, List, Toast, Picker } from 'native-base';
import CustomModal from '../Modal/Modal';
import IBaseListItem from '../../interfaces/IBaseListItem';
import ShoppingListDetailUDListItem from '../UDListItem/ShoppingListDetailUDListItem';
import { useGet, useMutation } from '../../hooks/useApi';
import IShoppingList from '../../interfaces/IShoppingList';
import IShop from '../../interfaces/IShop';

interface CUDListProps {
  apiPath: string;
  itemType: string;
  shareNavigate: () => void;
}

interface IItem {
  id: string;
  name: string;
  category: { id: string; name: string };
}

const ShoppingListDetailCUDList: React.FC<CUDListProps> = ({ apiPath, itemType, shareNavigate }) => {
  const [addModalVisible, toggleAddModal] = useState(false);
  const [addItem, setAddItem] = useState<IBaseListItem>();

  const [data, getData, loadingData] = useGet<IShoppingList>(apiPath);
  const [mutate] = useMutation<IBaseListItem>();
  const [items] = useGet<IBaseListItem[]>('/api/item');

  const [shopModalVisible, toggleShopModal] = useState(false);
  const [shops, getShops, loadingShops] = useGet<IShop[]>(`/api/store`);
  const [selectedShop, selectShop] = useState<IShop>();

  const [itemDefinitions, getItemDefinitions, loadingItems] = useGet<IItem[]>('/api/item');

  const closeAddModal = (): void => {
    toggleAddModal(false);
  };

  const handleSave = async () => {
    try {
      await mutate('POST', `${apiPath}/items`, { id: addItem?.id ?? '' });
    } catch (e) {
      Toast.show({ text: `Unable to add a new ${itemType}`, duration: 3000 });
    } finally {
      toggleAddModal(false);
      getData();
    }
  };

  const handleUpdate = async (item: IBaseListItem) => {
    try {
      await mutate('PUT', `${apiPath}/items/${item.id}`, { checked: !item.checked });
    } catch (e) {
      Toast.show({ text: `Unable to update ${itemType}`, duration: 3000 });
    } finally {
      toggleAddModal(false);
      getData();
    }
  };

  const handleDelete = async (item: IBaseListItem) => {
    try {
      await mutate('DELETE', `${apiPath}/items/${item.id}`);
    } catch (e) {
      Toast.show({ text: `Unable to delete ${itemType}`, duration: 3000 });
    } finally {
      toggleAddModal(false);
      getData();
    }
  };

  const loading = loadingData || loadingShops || loadingItems;

  if (!loading && !selectedShop && shops) {
    selectShop(shops[0]);
  }

  // Order data according to shop
  let orderedData: IBaseListItem[] = [];
  if (selectedShop && data && data.items && itemDefinitions) {
    orderedData = selectedShop.categoryOrder.reduce((result: IBaseListItem[], categoryId: string): IBaseListItem[] => {
      data.items
        .filter((itemEntry) => {
          const itemDefinition = itemDefinitions.find((def) => def.id === itemEntry.id);
          return itemDefinition && itemDefinition.category.id === categoryId;
        })
        .forEach((item) => result.push(item));
      return result;
    }, []);
  }

  const updateShop = () => {
    toggleShopModal(false);
  };

  return (
    <>
      <Container>
        <Content>
          <List>
            {!loading &&
              orderedData &&
              orderedData.map((item) => (
                <ShoppingListDetailUDListItem
                  listItem={item}
                  deleteItem={handleDelete}
                  updateItem={handleUpdate}
                  key={item.id}
                />
              ))}
            {loading && <Text>Loading...</Text>}
          </List>
        </Content>
        <Footer>
          <Button large primary onPress={() => shareNavigate()}>
            <Icon name="people" />
          </Button>
          <Button large primary onPress={() => toggleAddModal(true)}>
            <Icon name="add" />
          </Button>
          <Button large primary onPress={() => toggleShopModal(true)}>
            <Icon name="cart" />
            <Text>{selectedShop?.name}</Text>
          </Button>
        </Footer>
      </Container>
      <CustomModal
        modalVisible={addModalVisible}
        modalTitle={`Add ${itemType}`}
        handleClose={closeAddModal}
        handleSave={handleSave}
      >
        <>
          {items && (
            <Picker
              selectedValue={addItem}
              mode="dropdown"
              placeholder="Select item"
              placeholderStyle={{ color: '#bfc6ea' }}
              placeholderIconColor="#007aff"
              onValueChange={(item) => setAddItem(item)}
              style={{ marginTop: 20, width: 200, height: 50 }}
            >
              {items.map((item) => (
                <Picker.Item label={item.name} value={item} key={item.id} />
              ))}
            </Picker>
          )}
        </>
      </CustomModal>
      <CustomModal
        modalVisible={shopModalVisible}
        modalTitle="Select Shop"
        handleClose={updateShop}
        handleSave={updateShop}
      >
        {shops && (
          <Picker
            selectedValue={selectedShop}
            mode="dropdown"
            placeholder="Select Shop"
            onValueChange={(item) => selectShop(item)}
            style={{ marginTop: 20, width: 200, height: 50 }}
          >
            {shops.map((shop) => (
              <Picker.Item label={shop.name} value={shop} key={shop.id} />
            ))}
          </Picker>
        )}
      </CustomModal>
    </>
  );
};

export default ShoppingListDetailCUDList;
