import { StyleSheet } from 'react-native';

const style = StyleSheet.create({
  modalContent: {
    minWidth: '80%',
    maxWidth: '80%',
    height: 40,
  },
  modalInput: {
    width: '100%',
  },
});

export default style;
