import React, { useState } from 'react';
import { Button, Text, Container, Content, Footer, Icon, List, Toast } from 'native-base';
import { TextInput } from 'react-native';
import CustomModal from '../Modal/Modal';
import style from './CUDList.style';
import IBaseListItem from '../../interfaces/IBaseListItem';
import { useGet, useMutation } from '../../hooks/useApi';
import UDListItem from '../UDListItem/UDListItem';

interface CUDListProps {
  apiPath: string;
  itemType: string;
  onItemClick: (item: IBaseListItem) => void;
}

export interface Item {
  id: string;
  name: string;
}

const ShoppingListCUDList: React.FC<CUDListProps> = ({ apiPath, itemType, onItemClick }) => {
  const [addModalVisible, toggleAddModal] = useState(false);
  const [addName, setAddName] = useState('');

  const [items, getItems, loading] = useGet<Item[]>(apiPath);
  const [mutate] = useMutation<IBaseListItem>();

  const closeAddModal = (): void => {
    setAddName('');
    toggleAddModal(false);
  };

  const handleSave = async () => {
    try {
      await mutate('POST', apiPath, { name: addName });
    } catch (e) {
      Toast.show({ text: `Unable to add a new ${itemType}`, duration: 3000 });
    } finally {
      setAddName('');
      toggleAddModal(false);
      getItems();
    }
  };

  const handleUpdate = async (item: IBaseListItem) => {
    try {
      await mutate('PUT', `${apiPath}/${item.id}`, { name: item.name });
    } catch (e) {
      Toast.show({ text: `Unable to update ${itemType}`, duration: 3000 });
    } finally {
      setAddName('');
      toggleAddModal(false);
      getItems();
    }
  };

  const handleDelete = async (item: IBaseListItem) => {
    try {
      await mutate('DELETE', `${apiPath}/${item.id}`);
    } catch (e) {
      Toast.show({ text: `Unable to delete ${itemType}`, duration: 3000 });
    } finally {
      setAddName('');
      toggleAddModal(false);
      getItems();
    }
  };

  return (
    <>
      <Container>
        <Content>
          <List>
            {items &&
              items.map((item) => (
                <UDListItem
                  listItem={item}
                  deleteItem={handleDelete}
                  updateItem={handleUpdate}
                  modalTitle={`Update ${itemType}`}
                  key={item.id}
                  onItemClick={onItemClick}
                />
              ))}
            {loading && <Text>Loading...</Text>}
          </List>
        </Content>
        <Footer>
          <Button large primary onPress={() => toggleAddModal(true)}>
            <Icon name="add" />
          </Button>
        </Footer>
      </Container>
      <CustomModal
        modalVisible={addModalVisible}
        modalTitle={`Add ${itemType}`}
        handleClose={closeAddModal}
        handleSave={handleSave}
      >
        <>
          <TextInput
            onChangeText={setAddName}
            value={addName}
            style={style.modalInput}
            placeholder={`New ${itemType} name`}
            autoFocus
          />
        </>
      </CustomModal>
    </>
  );
};

export default ShoppingListCUDList;
