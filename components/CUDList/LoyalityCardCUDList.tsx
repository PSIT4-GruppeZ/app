import React, { useState } from 'react';
import { Button, Text, Container, Content, Footer, Icon, List, Toast } from 'native-base';
import { TextInput } from 'react-native';
import CustomModal from '../Modal/Modal';
import style from './CUDList.style';
import ILoyalityCardListItem from '../../interfaces/ILoyalityCardListItem';
import { useGet, useMutation } from '../../hooks/useApi';
import LoyalityCardUDListItem from '../UDListItem/LoyalityCardUDListItem';

interface CUDListProps {
  apiPath: string;
  itemType: string;
  onItemClick: (item: ILoyalityCardListItem) => void;
}

export interface LoyalityCard {
  id: string;
  name: string;
  barcode: string;
}

const LoyalityCardCUDList: React.FC<CUDListProps> = ({ apiPath, itemType, onItemClick }) => {
  const [addModalVisible, toggleAddModal] = useState(false);
  const [addName, setAddName] = useState('');
  const [addBarcode, setAddBarcode] = useState('');

  const [loyalityCards, getLoyalityCard, loading] = useGet<LoyalityCard[]>(apiPath);
  const [mutate] = useMutation<ILoyalityCardListItem>();

  const closeAddModal = (): void => {
    setAddName('');
    toggleAddModal(false);
  };

  const handleSave = async () => {
    try {
      await mutate('POST', apiPath, { name: addName, barcode: addBarcode });
    } catch (e) {
      Toast.show({ text: `Unable to add a new ${itemType}`, duration: 3000 });
    } finally {
      setAddName('');
      setAddBarcode('');
      toggleAddModal(false);
      getLoyalityCard();
    }
  };

  const handleUpdate = async (item: ILoyalityCardListItem) => {
    try {
      await mutate('PUT', `${apiPath}/${item.id}`, { name: item.name, barcode: item.barcode });
    } catch (e) {
      Toast.show({ text: `Unable to update ${itemType}`, duration: 3000 });
    } finally {
      setAddName('');
      setAddBarcode('');
      toggleAddModal(false);
      getLoyalityCard();
    }
  };

  const handleDelete = async (item: ILoyalityCardListItem) => {
    try {
      await mutate('DELETE', `${apiPath}/${item.id}`);
    } catch (e) {
      Toast.show({ text: `Unable to delete ${itemType}`, duration: 3000 });
    } finally {
      setAddName('');
      toggleAddModal(false);
      getLoyalityCard();
    }
  };

  return (
    <>
      <Container>
        <Content>
          <List>
            {loyalityCards &&
              loyalityCards.map((loyalityCard) => (
                <LoyalityCardUDListItem
                  listItem={loyalityCard}
                  deleteItem={handleDelete}
                  updateItem={handleUpdate}
                  modalTitle={`Update ${itemType}`}
                  key={loyalityCard.id}
                  onItemClick={onItemClick}
                />
              ))}
            {loading && <Text>Loading...</Text>}
          </List>
        </Content>
        <Footer>
          <Button large primary onPress={() => toggleAddModal(true)}>
            <Icon name="add" />
          </Button>
        </Footer>
      </Container>
      <CustomModal
        modalVisible={addModalVisible}
        modalTitle={`Add ${itemType}`}
        handleClose={closeAddModal}
        handleSave={handleSave}
      >
        <>
          <TextInput
            onChangeText={setAddName}
            value={addName}
            style={style.modalInput}
            placeholder={`New ${itemType} name`}
            autoFocus
          />
          <TextInput
            onChangeText={setAddBarcode}
            value={addBarcode}
            style={style.modalInput}
            placeholder="New barcode"
          />
        </>
      </CustomModal>
    </>
  );
};

export default LoyalityCardCUDList;
