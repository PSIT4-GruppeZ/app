import React, { useState } from 'react';
import { Button, Text, Container, Content, Footer, Icon, List, Toast, Picker } from 'native-base';
import CustomModal from '../Modal/Modal';
import IBaseListItem from '../../interfaces/IBaseListItem';
import { useGet, useMutation } from '../../hooks/useApi';
import ICartUser from '../../interfaces/ICartUser';
import IShoppingList from '../../interfaces/IShoppingList';
import ShoppingListUsersUDListItem from '../UDListItem/ShoppingListUsersUDListItem';

interface CUDListProps {
  apiPath: string;
  itemType: string;
}

const ShoppingListUsersCUDList: React.FC<CUDListProps> = ({ apiPath, itemType }) => {
  const [addModalVisible, toggleAddModal] = useState(false);
  const [addUser, setAddUser] = useState<IBaseListItem>();

  const [data, getData, loading] = useGet<IShoppingList>(apiPath);
  const [mutate] = useMutation<IBaseListItem>();
  const [users] = useGet<ICartUser[]>('/api/user');

  const closeAddModal = (): void => {
    toggleAddModal(false);
  };

  const handleSave = async () => {
    try {
      await mutate('POST', `${apiPath}/share`, { userid: addUser?.id ?? '' });
    } catch (e) {
      Toast.show({ text: `Unable to add a new ${itemType}`, duration: 3000 });
    } finally {
      toggleAddModal(false);
      getData();
    }
  };

  const handleDelete = async (item: ICartUser) => {
    try {
      await mutate('DELETE', `${apiPath}/share/${item.id}`);
    } catch (e) {
      Toast.show({ text: `Unable to delete ${itemType}`, duration: 3000 });
    } finally {
      toggleAddModal(false);
      getData();
    }
  };

  return (
    <>
      <Container>
        <Content>
          <List>
            {data &&
              data.users &&
              data.users.map((user) => (
                <ShoppingListUsersUDListItem listItem={user} deleteItem={handleDelete} key={user.id} />
              ))}
            {loading && <Text>Loading...</Text>}
          </List>
        </Content>
        <Footer>
          <Button large primary onPress={() => toggleAddModal(true)}>
            <Icon name="add" />
          </Button>
        </Footer>
      </Container>
      <CustomModal
        modalVisible={addModalVisible}
        modalTitle={`Add ${itemType}`}
        handleClose={closeAddModal}
        handleSave={handleSave}
      >
        <>
          {users && (
            <Picker
              selectedValue={addUser}
              mode="dropdown"
              placeholder="Select user"
              placeholderStyle={{ color: '#bfc6ea' }}
              placeholderIconColor="#007aff"
              onValueChange={(user) => setAddUser(user)}
              style={{ marginTop: 20, width: 200, height: 50 }}
            >
              {users.map((user) => (
                <Picker.Item label={`${user.username} / ${user.email}`} value={user} key={user.id} />
              ))}
            </Picker>
          )}
        </>
      </CustomModal>
    </>
  );
};

export default ShoppingListUsersCUDList;
