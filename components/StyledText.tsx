import * as React from 'react';

import { Text, TextProps } from './Themed';

const MonoText: React.FC<TextProps> = (props: TextProps) => {
  const { style } = props;
  return <Text {...props} style={[style, { fontFamily: 'space-mono' }]} />;
};

export default MonoText;
