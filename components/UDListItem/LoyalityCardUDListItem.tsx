import React, { useState } from 'react';
import { Col, Text, Grid, Icon, ListItem } from 'native-base';
import { TextInput } from 'react-native';
import style from './UDListItem.style';
import CustomModal from '../Modal/Modal';
import ILoyalityCardListItem from '../../interfaces/ILoyalityCardListItem';

interface ListItemProps {
  listItem: ILoyalityCardListItem;
  modalTitle: string;
  updateItem?: (listItem: ILoyalityCardListItem) => Promise<void>;
  deleteItem?: (listItem: ILoyalityCardListItem) => void;
  onItemClick?: (listItem: ILoyalityCardListItem) => void;
}

const LoyalityCardUDListItem: React.FC<ListItemProps> = (props) => {
  const { listItem, modalTitle, updateItem, deleteItem, onItemClick } = props;
  const [updateModalVisible, toggleUpdateModal] = useState(false);
  const [updateTitle, setUpdateTitle] = useState('');
  const [barcode, setBarcode] = useState('');

  const openUpdateModal = (item: ILoyalityCardListItem): void => {
    setUpdateTitle(item.name);
    setBarcode(item.barcode);
    toggleUpdateModal(true);
  };

  const closeUpdateModal = (): void => {
    toggleUpdateModal(false);
  };

  const update = async (): Promise<void> => {
    if (updateItem) {
      const uItem = { ...listItem, name: updateTitle };
      await updateItem(uItem);
      closeUpdateModal();
    }
  };

  const onItemPress = (): void => {
    if (onItemClick) {
      onItemClick(listItem);
    }
  };

  return (
    <ListItem>
      <Grid>
        <Col style={style.container}>
          <Text style={style.leftAlign} onPress={onItemPress}>
            {listItem.name}
          </Text>
        </Col>
        {updateItem && (
          <Col style={style.iconCol}>
            <Icon name="pencil" onPress={() => openUpdateModal(listItem)} />
          </Col>
        )}
        {deleteItem && (
          <Col style={style.iconCol}>
            <Icon name="trash" onPress={() => deleteItem(listItem)} />
          </Col>
        )}
      </Grid>
      <CustomModal
        modalVisible={updateModalVisible}
        modalTitle={modalTitle}
        handleClose={closeUpdateModal}
        handleSave={update}
      >
        <>
          <TextInput
            onChangeText={setUpdateTitle}
            value={updateTitle}
            style={style.modalInput}
            placeholder={`Rename ${listItem.name}`}
            autoFocus
          />
          <TextInput onChangeText={setBarcode} value={barcode} style={style.modalInput} placeholder="Change barcode" />
        </>
      </CustomModal>
    </ListItem>
  );
};

export default LoyalityCardUDListItem;
