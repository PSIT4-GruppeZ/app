import React, { useState } from 'react';
import { Col, Text, Grid, Icon, ListItem } from 'native-base';
import { TextInput } from 'react-native';
import style from './UDListItem.style';
import CustomModal from '../Modal/Modal';
import IBaseListItem from '../../interfaces/IBaseListItem';

interface ListItemProps {
  listItem: IBaseListItem;
  modalTitle: string;
  updateItem?: (listItem: IBaseListItem) => Promise<void>;
  deleteItem?: (listItem: IBaseListItem) => void;
  onItemClick?: (listItem: IBaseListItem) => void;
}

const UDListItem: React.FC<ListItemProps> = (props) => {
  const { listItem, modalTitle, updateItem, deleteItem, onItemClick } = props;
  const [updateModalVisible, toggleUpdateModal] = useState(false);
  const [updateTitle, setUpdateTitle] = useState('');

  const openUpdateModal = (item: IBaseListItem): void => {
    setUpdateTitle(item.name);
    toggleUpdateModal(true);
  };

  const closeUpdateModal = (): void => {
    toggleUpdateModal(false);
  };

  const update = async (): Promise<void> => {
    if (updateItem) {
      const uItem = { ...listItem, name: updateTitle };
      await updateItem(uItem);
      closeUpdateModal();
    }
  };

  const onItemPress = (): void => {
    if (onItemClick) {
      onItemClick(listItem);
    }
  };

  return (
    <ListItem>
      <Grid>
        <Col style={style.container}>
          <Text style={listItem.checked ? style.checked : style.leftAlign} onPress={onItemPress}>
            {listItem.name}
          </Text>
        </Col>
        {updateItem && (
          <Col style={style.iconCol}>
            <Icon name="pencil" onPress={() => openUpdateModal(listItem)} />
          </Col>
        )}
        {deleteItem && (
          <Col style={style.iconCol}>
            <Icon name="trash" onPress={() => deleteItem(listItem)} />
          </Col>
        )}
      </Grid>
      <CustomModal
        modalVisible={updateModalVisible}
        modalTitle={modalTitle}
        handleClose={closeUpdateModal}
        handleSave={update}
      >
        <>
          <TextInput
            onChangeText={setUpdateTitle}
            value={updateTitle}
            style={style.modalInput}
            placeholder={`Rename ${listItem.name}`}
            autoFocus
          />
        </>
      </CustomModal>
    </ListItem>
  );
};

export default UDListItem;
