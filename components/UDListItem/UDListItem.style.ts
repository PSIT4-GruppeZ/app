import { StyleSheet } from 'react-native';

const style = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-evenly',
    alignContent: 'flex-start',
  },
  iconCol: {
    width: 40,
  },
  leftAlign: {
    alignSelf: 'flex-start',
  },
  modalInput: {
    width: '100%',
  },
  marginTop: {
    marginTop: 20,
  },
  checked: {
    alignSelf: 'flex-start',
    textDecorationLine: 'line-through',
    textDecorationStyle: 'solid',
  },
});

export default style;
