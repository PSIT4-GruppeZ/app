import React, { useState } from 'react';
import { Col, Text, Grid, Icon, ListItem, Picker } from 'native-base';
import { TextInput } from 'react-native';
import style from './UDListItem.style';
import CustomModal from '../Modal/Modal';
import IBaseListItem from '../../interfaces/IBaseListItem';
import { Item } from '../CUDList/ItemsCUDList';

interface ListItemProps {
  listItem: Item;
  modalTitle: string;
  categories: IBaseListItem[];
  updateItem: (listItem: IBaseListItem, category?: IBaseListItem) => Promise<void>;
  deleteItem: (listItem: IBaseListItem) => void;
}

const ItemsUDListItem: React.FC<ListItemProps> = (props) => {
  const { listItem, modalTitle, categories, updateItem, deleteItem } = props;
  const [updateModalVisible, toggleUpdateModal] = useState(false);
  const [updateTitle, setUpdateTitle] = useState('');
  const [category, setCategory] = useState<IBaseListItem | undefined>(
    categories.find((c) => c.id === listItem.category.id),
  );

  const openUpdateModal = (item: IBaseListItem): void => {
    setUpdateTitle(item.name);
    toggleUpdateModal(true);
  };

  const closeUpdateModal = (): void => {
    toggleUpdateModal(false);
  };

  const update = async (): Promise<void> => {
    const uItem = { ...listItem, name: updateTitle };
    await updateItem(uItem, category);
    closeUpdateModal();
  };

  return (
    <ListItem>
      <Grid>
        <Col style={style.container}>
          <Text style={style.leftAlign}>{listItem.name}</Text>
        </Col>
        <Col style={style.iconCol}>
          <Icon name="pencil" onPress={() => openUpdateModal(listItem)} />
        </Col>
        <Col style={style.iconCol}>
          <Icon name="trash" onPress={() => deleteItem(listItem)} />
        </Col>
      </Grid>
      <CustomModal
        modalVisible={updateModalVisible}
        modalTitle={modalTitle}
        handleClose={closeUpdateModal}
        handleSave={update}
      >
        <>
          <TextInput
            onChangeText={setUpdateTitle}
            value={updateTitle}
            style={style.modalInput}
            placeholder={`Rename ${listItem.name}`}
            autoFocus
          />
          <Picker
            selectedValue={category}
            mode="dropdown"
            placeholder="Select category"
            placeholderStyle={{ color: '#bfc6ea' }}
            placeholderIconColor="#007aff"
            onValueChange={(item) => setCategory(item)}
            style={{ marginTop: 20, width: 200, height: 50 }}
          >
            {categories.map((item) => (
              <Picker.Item label={item.name} value={item} key={item.id} />
            ))}
          </Picker>
        </>
      </CustomModal>
    </ListItem>
  );
};

export default ItemsUDListItem;
