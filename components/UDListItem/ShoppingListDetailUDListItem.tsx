import React from 'react';
import { Col, Text, Grid, Icon, ListItem } from 'native-base';
import style from './UDListItem.style';
import IBaseListItem from '../../interfaces/IBaseListItem';

interface ListItemProps {
  listItem: IBaseListItem;
  updateItem?: (listItem: IBaseListItem) => Promise<void>;
  deleteItem?: (listItem: IBaseListItem) => void;
  onItemClick?: (listItem: IBaseListItem) => void;
}

const ShoppingListDetailUDListItem: React.FC<ListItemProps> = (props) => {
  const { listItem, updateItem, deleteItem, onItemClick } = props;

  const update = async (): Promise<void> => {
    if (updateItem) {
      const uItem = { ...listItem };
      await updateItem(uItem);
    }
  };

  const onItemPress = (): void => {
    if (onItemClick) {
      onItemClick(listItem);
    }
  };

  return (
    <ListItem>
      <Grid>
        <Col style={style.container}>
          <Text style={listItem.checked ? style.checked : style.leftAlign} onPress={onItemPress}>
            {listItem.name}
          </Text>
        </Col>
        {updateItem && (
          <Col style={style.iconCol}>
            <Icon name="checkmark" onPress={() => update()} />
          </Col>
        )}
        {deleteItem && (
          <Col style={style.iconCol}>
            <Icon name="trash" onPress={() => deleteItem(listItem)} />
          </Col>
        )}
      </Grid>
    </ListItem>
  );
};

export default ShoppingListDetailUDListItem;
