import React from 'react';
import { Col, Text, Grid, Icon, ListItem } from 'native-base';
import style from './UDListItem.style';
import ICartUser from '../../interfaces/ICartUser';

interface ListItemProps {
  listItem: ICartUser;
  deleteItem?: (listItem: ICartUser) => void;
}

const ShoppingListUsersUDListItem: React.FC<ListItemProps> = (props) => {
  const { listItem, deleteItem } = props;

  return (
    <ListItem>
      <Grid>
        <Col style={style.container}>
          <Text style={style.leftAlign}>
            {listItem.username} / {listItem.email}
          </Text>
        </Col>
        {deleteItem && (
          <Col style={style.iconCol}>
            <Icon name="trash" onPress={() => deleteItem(listItem)} />
          </Col>
        )}
      </Grid>
    </ListItem>
  );
};

export default ShoppingListUsersUDListItem;
