import React, { useState } from 'react';
import { Col, Text, Grid, Icon, ListItem } from 'native-base';
import { TextInput } from 'react-native';
import style from './UDListItem.style';
import CustomModal from '../Modal/Modal';
import IBaseListItem from '../../interfaces/IBaseListItem';
import { Item } from '../CUDList/ShoppingListCUDList';

interface ListItemProps {
  listItem: Item;
  modalTitle: string;
  updateItem: (listItem: IBaseListItem) => Promise<void>;
  deleteItem: (listItem: IBaseListItem) => void;
  onItemClick: (item: IBaseListItem) => void;
}

const ShoppingListUDListItem: React.FC<ListItemProps> = (props) => {
  const { listItem, modalTitle, updateItem, deleteItem, onItemClick } = props;
  const [updateModalVisible, toggleUpdateModal] = useState(false);
  const [updateTitle, setUpdateTitle] = useState('');

  const openUpdateModal = (item: IBaseListItem): void => {
    setUpdateTitle(item.name);
    toggleUpdateModal(true);
  };

  const closeUpdateModal = (): void => {
    toggleUpdateModal(false);
  };

  const update = async (): Promise<void> => {
    const uItem = { ...listItem, name: updateTitle };
    await updateItem(uItem);
    closeUpdateModal();
  };

  return (
    <ListItem>
      <Grid>
        <Col style={style.container} onTouchEnd={() => onItemClick(listItem)}>
          <Text style={style.leftAlign}>{listItem.name}</Text>
        </Col>
        <Col style={style.iconCol}>
          <Icon name="pencil" onPress={() => openUpdateModal(listItem)} />
        </Col>
        <Col style={style.iconCol}>
          <Icon name="trash" onPress={() => deleteItem(listItem)} />
        </Col>
      </Grid>
      <CustomModal
        modalVisible={updateModalVisible}
        modalTitle={modalTitle}
        handleClose={closeUpdateModal}
        handleSave={update}
      >
        <>
          <TextInput
            onChangeText={setUpdateTitle}
            value={updateTitle}
            style={style.modalInput}
            placeholder={`Rename ${listItem.name}`}
            autoFocus
          />
        </>
      </CustomModal>
    </ListItem>
  );
};

export default ShoppingListUDListItem;
