export default interface ICartUser {
  id: string;
  username: string;
  email: string;
}
