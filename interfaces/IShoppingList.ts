import IBaseListItem from './IBaseListItem';
import ICartUser from './ICartUser';

export default interface IShoppingList {
  id: string;
  name: string;
  items: IBaseListItem[];
  users: ICartUser[];
}
