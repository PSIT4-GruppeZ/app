import IBaseListItem from './IBaseListItem';

export default interface IShop extends IBaseListItem {
  categoryOrder: string[];
}
