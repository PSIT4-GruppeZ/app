export default interface IBaseListItem {
  id: string;
  name: string;
  checked?: boolean;
}
