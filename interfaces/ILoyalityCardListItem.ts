export default interface ILoyalityCardListItem {
  id: string;
  name: string;
  barcode: string;
}
