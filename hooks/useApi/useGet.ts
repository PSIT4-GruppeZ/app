import { useCallback, useEffect, useState } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { API_URL } from '../../config';

type GetResponse<T> = [data: T | undefined, getData: () => Promise<void>, loading?: boolean, error?: string];

const useGet = <T>(path: string): GetResponse<T> => {
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState<T | undefined>();
  const [error, setError] = useState<string>();

  const getData = useCallback(async () => {
    try {
      setLoading(true);
      const token = await AsyncStorage.getItem('token');
      const result = await fetch(API_URL + path, {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${token}`,
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      });
      if (result.status >= 400) {
        setError('POST Request failed');
        if (result.status === 401) {
          // handle unauthorized
        }
      }
      const response = await result.json();
      setData(response);
    } catch (e) {
      setError('GET Request failed');
    } finally {
      setLoading(false);
    }
  }, [path]);

  useEffect(() => {
    getData();
  }, [getData]);
  return [data, getData, loading, error];
};

export default useGet;
