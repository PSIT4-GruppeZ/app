import { useCallback, useState } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { API_URL } from '../../config';

type StringObject =
  | {
      [id: string]: string | boolean;
    }
  | any;

type MutationResponse<T> = [
  mutateData: (method: HttpMethod, path: string, body?: StringObject) => Promise<void>,
  data?: T,
  error?: string,
];

type HttpMethod = 'POST' | 'PUT' | 'DELETE';

const useMutation = <T>(): MutationResponse<T> => {
  const [data, setData] = useState<T | undefined>();
  const [error, setError] = useState<string>();

  const mutateData = useCallback(async (method: HttpMethod, path: string, body?: StringObject) => {
    const token = await AsyncStorage.getItem('token');
    try {
      const response = await fetch(API_URL + path, {
        method,
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify(body),
      });
      if (response.status >= 400) {
        setError('POST Request failed');
        if (response.status === 401) {
          // handle unauthorized
        }
      } else {
        const json = await response.json();
        setData(json);
      }
    } catch (e) {
      setError('POST Request failed');
    }
  }, []);
  return [mutateData, data, error];
};

export default useMutation;
